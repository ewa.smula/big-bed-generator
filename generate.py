import gzip
import ntpath
import os
import subprocess
import sys
import urllib.request
from pathlib import Path

BIN_PATH = "bin/"


def transform(input_line):
    chromosome = input_line[2]
    chromosome_start = input_line[4]
    chromosome_end = input_line[5]
    name = input_line[12]
    score = input_line[11]
    strand = input_line[3]
    thick_start = input_line[6]
    thick_end = input_line[7]
    item_rgb = "255,0,0"
    block_count = input_line[8]
    block_starts = []
    block_sizes = []

    block_starts_tmp = input_line[9].split(",")
    block_ends_tmp = input_line[10].split(",")
    for i in range(0, int(block_count)):
        block_starts.append(str(int(block_starts_tmp[i]) - int(chromosome_start)))
        block_sizes.append(str(int(block_ends_tmp[i]) - int(block_starts_tmp[i])))
    output_line = [chromosome, chromosome_start, chromosome_end, name, score, strand, thick_start, thick_end, item_rgb,
                   block_count, ",".join(block_sizes), ",".join(block_starts)]
    return output_line


def downloadUcscFile(filename):
    if not os.path.isdir(BIN_PATH):
        os.makedirs(BIN_PATH)

    if not Path(BIN_PATH + filename).is_file():
        url = 'http://hgdownload.soe.ucsc.edu/admin/exe/linux.x86_64/' + filename
        print('Beginning file download ' + filename + ' (' + url + ')')

        urllib.request.urlretrieve(url, BIN_PATH + filename)
        os.chmod(BIN_PATH + filename, 0o755)


inputFile = sys.argv[1]
db = sys.argv[2]
basename = ntpath.basename(inputFile).replace(".txt.gz", "")

downloadUcscFile("fetchChromSizes")

chrom_sizes = basename + ".chrom.sizes"
with open(chrom_sizes, 'w') as output_chrom_sizes:
    print('Fetching chrom sizes: ' + chrom_sizes)
    subprocess.call([BIN_PATH + "fetchChromSizes", db], stdout=output_chrom_sizes)

chromosomes = {}
unkown_chromosomes = {}

with open(chrom_sizes) as f:
    lines = f.readlines()
    for line in lines:
        chromosomes[line.split("\t")[0]] = True

output_file_unsorted = basename + ".bed"

output = open(output_file_unsorted, "w")

print('Generating bed file: ' + output_file_unsorted)

if inputFile.endswith('gz'):
    with gzip.open(inputFile, 'rt') as hIN:
        for line in hIN:
            F = line.rstrip('\n').split('\t')
            output_line = transform(F)
            if output_line[0] in chromosomes:
                print("\t".join(output_line), file=output)
            else:
                if not output_line[0] in unkown_chromosomes:
                    print("Unknown chromosome '" + output_line[0] + "'")
                unkown_chromosomes[output_line[0]] = True
else:
    with open(inputFile) as f:
        lines = f.readlines()
        for line in lines:
            F = line.rstrip('\n').split('\t')
            output_line = transform(F)
            if output_line[0] in chromosomes:
                print("\t".join(output_line), file=output)
            else:
                if not output_line[0] in unkown_chromosomes:
                    print("Unknown chromosome '" + output_line[0] + "'")
                unkown_chromosomes[output_line[0]] = True

output.close()

downloadUcscFile("bedSort")

output_file_sorted = basename + "-sorted.bed"
print('Sorting bed file: ' + output_file_sorted)
subprocess.call([BIN_PATH + "bedSort", output_file_unsorted, output_file_sorted])

downloadUcscFile("bedToBigBed")

output_big_bed = basename + ".bb"

print('Creating big bed file: ' + output_big_bed)
subprocess.call([BIN_PATH + "bedToBigBed", output_file_sorted, chrom_sizes, output_big_bed])
